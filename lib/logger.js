var log = function() {
    var args = Array.prototype.slice.call(arguments);
    args.unshift("GENERATOR: ");
    console.log.apply(console, args);
}

module.exports = {
    debugTerminal: log,
    debug: log,
    warn: log,
    warnTerminal: log,
    error: log,
    errorTerminal: log
};
