var sgUtilsParser = require('./utils/html-parser-util');
var path = require('path');
var sgUtils = require('./utils/common-utils');
var minify = require('minify-content');
var UglifyJS = require("uglify-js");
var logger = require(path.join(process.cwd(), 'lib/logger'));
var PROD = false; //process.env.PROD && process.env.PROD.toString() == '1' || false;
var MINIFYJS = (process.env.MINIFYJS !== undefined) ? process.env.MINIFYJS.toString() == '1' : true;
var env = require(path.join(process.cwd(), 'lib/config/env'));
var buble = require('buble');

var g = {
	destFilename: (env().APP_NAME || 'app') + '.min.js'
}
var PATH = path.join(process.cwd(), 'src/public/js');
var DEST_FOLDER = path.join(process.cwd(), 'dist/js');

function watch() {
	sgUtils.watch(PATH, () => {
		build();
	});
}

function build() {
	var raw = sgUtils.concatenateAllFilesFrom(PATH, {
		debug: false
	});
	var result = buble.transform(raw); // { code: ..., map: ... }
	result = UglifyJS.minify(result.code);
	if (result.error) {
		return logger.error(result.error); // runtime error, or `undefined` if no error 
	}
	else {
		return build_next(result.code);
	}

	function build_next(_raw) {
		var dest = DEST_FOLDER + '/' + g.destFilename;
		sgUtils.createFile(dest, _raw);
		logger.debugTerminal('Scripts build ' + g.destFilename + ' success at ' + new Date());
		emit('build-success');
	}
}

function emit(evt, p) {
	_events[evt] = _events[evt] || [];
	_events[evt].forEach(handler => handler(p));
}
var _events = {};

function getAll() {
	return sgUtils.retrieveFilesFromPathSync(PATH);
}

function tagTemplate(context) {
	var rta = sgUtils.replaceAll('<script type="text/javascript" src="_SRC_"></script>', '_SRC_', context.src || '[src_field_required]');
	return rta;
}

function printTags(folderPath) {
	var files = getAll();
	var ret = "<!-- printJSTags: development -->\n";
	files.forEach((file) => {
		ret += tagTemplate({
			src: path.join(folderPath, file.fileName)
		});
	});
	return ret;
}

module.exports = {
	path: (p) => PATH = p,
	build: build,
	tagTemplate: tagTemplate,
	printTags: printTags,
	watch: watch,
	on: (evt, handler) => {
		_events[evt] = _events[evt] || [];
		_events[evt].push(handler);
	}
};
