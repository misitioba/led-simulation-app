var path = require('path');
var logger = require(path.join(process.cwd(), 'lib/logger'));
var heTpls = require('./static-compiler');
var sander = require('sander');
var Promise = require("promise");
var env = require(path.join(process.cwd(), 'lib/config/env'));
var jsCompiler = require(path.join(process.cwd(), 'lib/js-compiler'));
var gulp = require('gulp');
var sass = require('gulp-sass');
var fs = require('fs');
var sourcemaps = require('gulp-sourcemaps');
module.exports = {
    configure: configure
};

function configure(app) {
    return new Promise((resolve, reject, emit) => {

        if (env().PROD) {
            jsCompiler.build();
        }

        //Styles


        gulp.task('sass', function() {
            var mainPath = path.join(process.cwd(), 'src/public/css/main.scss');
            if (!fs.existsSync(mainPath)) {
                return console.log('main.scss not found at src/public/css');
            }
            var options = {};
            if(env().PROD){
                options.outputStyle= 'compressed';
            }
            return gulp.src(mainPath)
            .pipe(sourcemaps.init())
                .pipe(sass(options).on('error', sass.logError))
                .pipe(sourcemaps.write())
                .pipe(gulp.dest(path.join(process.cwd(), 'dist/css')));
        });
        gulp.start('sass');



        //console.log('Watching..');
        //heTpls.watch();
        //console.log('First build..');
        heTpls.build().then(() => {
            logger.debugTerminal('Build all success');
            resolve();
        });
    });
}
