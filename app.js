var PORT = process.env.PORT || 8080;
var path = require("path");
var express = require('express');
var app = express();
var http = require('http');
var path = require('path');
//console.log('Starting...');
app.use("/", express.static(path.join(process.cwd(), 'src/public')));
app.use("/dist", express.static(path.join(process.cwd(), 'dist')));
app.use("/dist/vendor", express.static(path.join(process.cwd(), 'vendor')));
require(path.join(process.cwd(), 'lib/bootstrap')).configure(app).then(() => {
	var server = http.createServer(app);
	server.listen(PORT, function() {
		console.log('Listening on', 'http://localhost:' + PORT);
	});
});
