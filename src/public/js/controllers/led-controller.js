(function() {
    /*global angular, _, $*/

    angular.module('app').filter('ledValue', function() {
        return function(input, symbol) {
            return parseFloat(input).toFixed(0);
        };
    });
    angular.module('app').filter('twoDecimals', function() {
        return function(input, symbol) {
            return parseFloat(input).toFixed(0);
        };
    });
    angular.module('app').filter('euros', function() {
        var div = $("<div/>");
        return function(input, symbol) {
            return input + ' ' + div.html("&#x20AC;").text();
        };
    });
    angular.module('app').filter('kgs', function() {
        return function(input, symbol) {
            return input + ' ' + "KGs";
        };
    });
    angular.module('app').filter('kwh', function() {
        return function(input, symbol) {
            return input + ' ' + "kWh";
        };
    });
    angular.module('app').filter('days', function() {
        return function(input, symbol) {
            return input + ' ' + "jours";
        };
    });
    angular.module('app').filter('years', function() {
        return function(input, symbol) {
            return parseFloat(input).toFixed(1) + ' ' + "ans";
        };
    })

    .directive('wrapHeight', ['$log', '$timeout', function($log, $timeout) {
        return {
            restrict: "A",
            scope: false,
            link: function(scope, element, attrs) {
                element.parent().find(attrs.wrapHeight);
                if (!attrs.wrapHeight) return $log.error('wrapHeight expects selector')

                function autoResize() {
                    element.outerHeight(element.parent().find(attrs.wrapHeight).outerHeight());
                }
                angular.element(function() {
                    autoResize();
                    $timeout(autoResize, 1000);
                });
                $(window).on('resize', autoResize);
            }
        }
    }])

    .directive('radio', ['$log', '$parse', '$timeout', function($log, $parse, $timeout) {
        return {
            restrict: "A",
            scope: false,
            link: function(scope, element, attrs) {
                var id = 'radio-' + window.btoa(attrs.radio);
                var evaluatedValue = scope.$eval(attrs.radioValue);
                var getter = $parse(attrs.radio);
                var setter = getter.assign;
                element.parent().find('input').attr('checked', evaluatedValue == getter(scope));
                scope.$on(id, function() {
                    element.parent().find('input').attr('checked', false);
                });
                element.on('click', function() {
                    scope.$emit(id);
                    element.parent().find('input').attr('checked', true);
                    setter(scope, evaluatedValue);
                    $timeout(function() {
                        scope.$apply();
                    });
                });
            }
        };
    }])

    .service('$Modal', function() {
        var self = {};
        var modal = new window.tingle.modal({
            footer: true,
            stickyFooter: false,
            closeMethods: ['overlay', 'button', 'escape'],
            closeLabel: "Close",
            cssClass: ['ldm-modal'],
            onOpen: function() {

            },
            onClose: function() {

            },
            beforeClose: function() {
                // here's goes some logic
                // e.g. save content before closing the modal
                return true; // close the modal
                //return false; // nothing happens
            }
        });

        // set content
        modal.setContent('<h1 class="ldm-center">Merci de bien remplir tous les champs</h1>');
        modal.addFooterBtn('OK', 'ldm-center-block tingle-btn tingle-btn--primary', function() {
            modal.close();
        });
        self.show = () => modal.open();
        self.hide = () => modal.close();
        return self;
    })

    .controller('led-controller', ['$scope', '$rootScope', '$log', 'snippets', '$timeout', '$Modal', function($scope, $rootScope, $log, snippets, $timeout, $Modal) {
        snippets.exposeGlobal('s', $scope);
        const PRICE_PER_WEEK_DEFAULT = 0.14;
        const RENT_MONTH_RATE_DEFAULT = 3.39;
        const CALCULATION_TIME = 2000;
        var calculationDone = false,
            isCalculating = false;
        $scope.fields = {
            halogen: {
                power: null,
                lifetime: null,
                price: null,
                quantity: null,
            },
            led: {
                power: null,
                lifetime: null,
                price: null,
                quantity: null,
            },
            isBuy: true,
            hoursPerDay: null,
            daysPerWeek: null,
            pricePerKwh: PRICE_PER_WEEK_DEFAULT
        };


        function inputFieldsFilled() {
            var f = $scope.fields;
            if (!f.halogen.power) return false;
            if (!f.halogen.lifetime) return false;
            if (!f.halogen.price) return false;
            if (!f.halogen.quantity) return false;
            if (!f.led.power) return false;
            if (!f.led.lifetime) return false;
            if (!f.led.price) return false;
            if (!f.led.quantity) return false;
            if (!f.hoursPerDay) return false;
            if (!f.daysPerWeek) return false;
            if (!f.pricePerKwh) return false;
            return true;
        }

        var defaults = _.cloneDeep($scope.fields);

        $scope.monthlyRentingRate = {
            sixty: 2.18,
            fortyEight: 2.63,
            thirtySix: 3.39
        };
        $scope.selectors = {
            monthsDuration: RENT_MONTH_RATE_DEFAULT
        };




        $scope.results = {
            hy: null,
            nyHalogen: null,
            nyLed: null,
            pyHalogen: null,
            pyLed: null,
            savings: {
                A: null,
                B: null,
                C: null,
                D: null
            },
            halogen: {
                A: null,
                B: null,
                C: null,
                D: null
            },
            led: {
                A: null,
                B: null,
                C: null,
                D: null
            }
        };
        $scope.isResultView = function() {
            return calculationDone;
        };
        $scope.isResultViewRentMode = function() {
            return calculationDone && !$scope.fields.isBuy;
        };
        $scope.isResultViewBuyMode = function() {
            return calculationDone && $scope.fields.isBuy;
        };
        $scope.isPrepareView = function() {
            return !calculationDone;
        };
        $scope.showLoadingWrapper = function() {
            return isCalculating;
        };


        $scope.test = function() {
            var f = $scope.fields;
            f.halogen.power = 50;
            f.led.power = 7;
            f.halogen.lifetime = 5000;
            f.led.lifetime = 30000;
            f.halogen.price = 7;
            f.led.price = 12;
            f.halogen.quantity = 30;
            f.led.quantity = 30;
            f.hoursPerDay = 8;
            f.daysPerWeek = 5;
            $rootScope.dom();
        };

        function normalizeNumber(n) {
            return isNaN(n) ? 0 : n;
        }

        $scope.ledPrix = () => $scope.fields.led.price * $scope.fields.led.quantity;
        $scope.ledCoveredDays = () => {
            if (!$scope.results.numberOfYearsToRefund) return 0;
            return $scope.results.numberOfYearsToRefund * 365;
        };
        $scope.yearsOfSaving = () => {
            if (!$scope.results.numberOfYearsToRefund) return $scope.results.nyLed || 0;
            return $scope.results.nyLed - $scope.results.numberOfYearsToRefund;
        };
        $scope.fiveYearsSavings = () => {
            if (!$scope.results.savings.C) return 0;
            return ($scope.results.savings.C * 5) - $scope.ledPrix();
        };
        $scope.rentPrixPerYear = () => {
            return normalizeNumber((($scope.fields.led.price * $scope.fields.led.quantity) * ($scope.selectors.monthsDuration * 0.01) * 12));
        };
        $scope.rentYearSaving = () => {
            return normalizeNumber($scope.results.savings.C - $scope.rentPrixPerYear());
        };

        $scope.calculate = function() {

            if (!inputFieldsFilled()) {
                return $Modal.show();
            }

            if (!$scope.fields.isBuy && !$scope.selectors.monthsDuration) {
                return alert("Sélectionner un certain nombre de mois");
            }
            var f = $scope.fields;
            var h = f.halogen;
            var l = f.led;
            $scope.results.hy = normalizeNumber(f.hoursPerDay * f.daysPerWeek * 52);
            $scope.results.nyHalogen = normalizeNumber((h.lifetime / $scope.results.hy));
            $scope.results.pyHalogen = normalizeNumber((h.price * h.quantity) / $scope.results.nyHalogen);
            $scope.results.nyLed = normalizeNumber((l.lifetime / $scope.results.hy));
            $scope.results.pyLed = normalizeNumber((l.price * l.quantity) / $scope.results.nyLed);
            $scope.results.halogen.A = (h.power * h.quantity * f.hoursPerDay * f.daysPerWeek * 52) / 1000;
            $scope.results.led.A = (l.power * l.quantity * f.hoursPerDay * f.daysPerWeek * 52) / 1000;
            $scope.results.halogen.B = $scope.results.halogen.A * 1.5;
            $scope.results.led.B = $scope.results.led.A * 1.5;
            $scope.results.halogen.C = $scope.results.halogen.A * f.pricePerKwh;
            $scope.results.led.C = $scope.results.led.A * f.pricePerKwh;
            /*
            $scope.results.halogen.D = normalizeNumber($scope.results.pyHalogen + $scope.results.halogen.C);
            if (f.isBuy) {
                $scope.results.led.D = normalizeNumber($scope.results.pyLed + $scope.results.led.C);
            }
            else {
                $scope.results.led.D = normalizeNumber($scope.results.led.C + ((l.price * l.quantity) * ($scope.selectors.monthsDuration * 0.01) * 12));
            }
            */
            $scope.results.savings.A = $scope.results.halogen.A - $scope.results.led.A;
            $scope.results.savings.B = $scope.results.halogen.B - $scope.results.led.B;
            $scope.results.savings.C = $scope.results.halogen.C - $scope.results.led.C;

            $scope.results.numberOfYearsToRefund = $scope.ledPrix() / $scope.results.savings.C;

            //$scope.results.savings.D = $scope.results.halogen.D - $scope.results.led.D;

            isCalculating = true;
            $timeout(function() {
                isCalculating = false;
                calculationDone = true;
                $rootScope.dom();
            }, CALCULATION_TIME);
        };
        $scope.reset = function() {
            $scope.fields = _.cloneDeep(defaults, true);
            _.each($scope.results, h => _.each(h, (v, k) => h[k] = null));
            calculationDone = false;
            $rootScope.dom();
        };
        $scope.goBack = function() {
            calculationDone = false;
            $rootScope.dom();
        };
        $scope.contact = function() {
            if (window.ledSimulationContactHandler) {
                return window.ledSimulationContactHandler();
            }
            else {
                if (window.top.window.ledSimulationContactHandler) {
                    return window.top.ledSimulationContactHandler();
                }
            }
            return $log.warn('ledSimulationContactHandler function required');
        };


    }]);
})();
